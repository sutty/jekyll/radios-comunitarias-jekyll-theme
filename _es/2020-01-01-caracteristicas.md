---
layout: post
title: Características de la plantilla
uuid: ccebde41-7044-4082-afe6-7740c4a8d7ae
description: Todo lo que podés hacer con esta plantilla
image:
  path: public/placeholder.png
locales:
- 83b767fb-e174-4ae3-b5d3-8198f8165d93
---

¡Hola!  Este es un sitio de muestra para la plantilla de radios
comunitarias y alternativas de [Sutty](https://sutty.nl/), la plataforma
para alojar sitios seguros, rápidos y resilientes.

Desarrollamos esta plantilla en base a conversaciones que fuimos
teniendo con compas comunicadorxs, especialmente de la [Red Nacional de
Medios Alternativos](https://rnma.org.ar/).  La presentamos el 29 de
agosto de 2020 durante el [17mo Encuentro de la Comunicación
Comunitaria, Alternativa
y Popular](https://www.instagram.com/p/CEdF80hJj5e/), donde participamos
en la mesa de Software Libre.

## Características

Con esta plantilla pueden:

* Agregar todas las direcciones del _streaming_ de la radio, de forma
  que si una esta caída, la radio siga funcionando.

* Al navegar por el sitio la radio no se corta.

* En celulares Android, al cambiar de app o apagar la pantalla, la radio
  sigue reproduciendo, ¡pueden escuchar la radio con el celular en el
  bolsillo!

* Si hay un corte, la radio retoma la transmisión.

* Publicar artículos, ilustrados con imágenes.

* Subir su logo, presentación, redes sociales y cambiar los colores para
  adaptarlos a su identidad.

* Todos los artículos se pueden compartir a redes sociales.

* ¡Traducir el sitio a distintos idiomas!

## ¡Queremos usarla!

Como Sutty, esta plantilla es libre y gratuita de usar, solo tienen que
[registrarse en el panel](https://panel.sutty.nl/) y crear el sitio
siguiendo los pasos.  Sólo pedimos que respeten nuestros [términos de
servicio](https://sutty.nl/terminos-de-servicio/) y [código de
convivencia](https://sutty.nl/codigo-de-convivencia/), en resumen, que
sean proyectos de la comunicación popular anti-opresiva 😁

<a href="https://panel.sutty.nl/" class="btn btn-success btn-block">Crear mi radio</a>

Les compas de [Radios Libres](https://radioslibres.net/) nos hicieron
una
[entrevista](https://radioslibres.net/sutty-webs-libres-seguras-y-sostenibles-para-organizaciones-sociales/)
y escribieron una guía para crearse un sitio:

<a
  href="https://radioslibres.net/crea-la-web-de-tu-radio-online-en-sutty/"
  class="btn btn-success btn-block">Cómo crear una radio</a>

## No tenemos _streaming_

Si aún no tienen su servicio de streaming, también podemos ayudarte
a encontrar uno. Sugerimos contactar con lxs siguientes colectivxs:

<table class="table">
  <thead>
    <tr>
      <th>Colectivx</th>
      <th>Sitio</th>
      <th>Contacto</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Yanapak</td>
      <td><a href="https://yanapak.org/">https://yanapak.org/</a></td>
      <td><a href="mailto:contacto@yanapak.org">contacto@yanapak.org</a></td>
    </tr>
    <tr>
      <td>Numérica</td>
      <td><a href="https://numerica.latina.red">https://numerica.latina.red</a></td>
      <td><a href="mailto:correspondencia@latina.red">correspondencia@latina.red</a></td>
    </tr>
  </tbody>
</table>

## Economía solidaria

Sutty es parte de la economía solidaria y se sostiene en base
a proyectos de desarrollo específico y aportes voluntarios.  Esta
plantilla la desarrollamos con le compa
[Librenauta](https://copiona.com/) en 15 horas.

Tenemos un montón de ideas sobre cómo mejorar esta plantilla
y características nuevas y seguramente uds. tengan las propias también.

Nuestro objetivo es que entre todes podamos pensar cómo puede ser aliada
de la comunicación alternativa, dimensionando y apoyando el trabajo que
realizamos desde Sutty.

¡Les invitamos a probarla y ponerse en contacto con nosotres!

<a href="https://donaciones.sutty.nl/" class="btn btn-success btn-block">Aportes a Sutty</a>

<a href="https://sutty.nl/#contacto" class="btn btn-success btn-block">Contactanos</a>

