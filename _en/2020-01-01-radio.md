---
layout: radio
title: Community Radio
description: A Sutty theme for alternative and independent media
logo:
  path: public/placeholder.png
  description: Description
streams:
- https://orelha.radiolivre.org/radiopueblo.mp3
fediverse: https://todon.nl/@sutty
instagram: https://instagram.com/sutty.web
twitter: https://twitter.com/SuttyWeb
email: sutty@riseup.net
background_color: 'white'
foreground_color: '#241f31'
navbar_background_color: '#6dc381'
navbar_foreground_color: '#241f31'
social_foreground_color: 'white'
uuid: 4546badb-466e-4b54-98b7-be5ba724e6e9
locales:
- de2f9d6f-f3a5-4aea-91fb-2b69b6173e77
---

This theme is for community and alternative radios that are streaming
over the Internet.
